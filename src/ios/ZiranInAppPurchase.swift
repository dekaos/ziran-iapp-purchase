import Foundation
import StoreKit
import SwiftyStoreKit

@objc(ZiranInAppPurchaseIOS) class ZiranInAppPurchaseIOS: CDVPlugin {

    @objc(initTransactions:)
    func initTransactions (command: CDVInvokedUrlCommand) {
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored, .failed:
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                        // Unlock content
                case .purchasing, .deferred:
                    break // do nothing
                }
            }
        }

        let pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK
        )

        self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
        )
    }

    @objc(getProductInfo:)
    func getProductInfo (command: CDVInvokedUrlCommand) {
        let productId = command.arguments[0] as! String

        SwiftyStoreKit.retrieveProductsInfo([productId]) { result in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                let res = [
                    "productId": productId,
                    "description": product.localizedDescription,
                    "price": priceString, "status": "success"
                ] as [AnyHashable : Any]

                pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_OK,
                        messageAs: res
                )
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                let res = [
                    "productId": productId,
                    "description": "A identifcação do produto é inválida.",
                    "status": "error"
                ] as [AnyHashable : Any]

                pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_ERROR,
                        messageAs: res
                )
            }
            else {
                let res = [
                    "productId": productId,
                    "description": result.error,
                    "status": "error"
                ] as [AnyHashable : Any]

                pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_ERROR,
                        messageAs: res
                )
            }

            self.commandDelegate!.send(
                    pluginResult,
                    callbackId: command.callbackId
            )
        }
    }

    @objc (purchase:)
    func purchase (command: CDVInvokedUrlCommand) {
        let product = command.arguments[0] as! String
        var hasError = false;

        // key and value, key as string, value as any type we need,
        // but keep in mind these types need to be used with careful
        var res = [String: Any]()

        SwiftyStoreKit.purchaseProduct(product, quantity: 1, atomically: true) { result in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)

            switch result {
            case .success(let purchase):
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                res["productId"] = purchase.productId
                res["transactionId"] = purchase.transaction.transactionIdentifier
                res["purchaseState"] = "purchased"
                res["status"] = "success"

            case .error(let error):
                hasError = true
                switch error.code {
                case .unknown: res["status"] = "error"
                    res["errorDescription"] = "Erro ao processar pedido, por favor tente novamente."
                    res["purchaseState"] = "unknown"
                case .clientInvalid: res["status"] = "error"
                    res["errorDescription"] = "Cliente não tem autorização para fazer pagamentos."
                    res["purchaseState"] = "clientInvalid"
                case .paymentCancelled: res["errorDescription"] = "Usuário cancelou a requisição.";
                    res["status"] = "canceled"
                    res["purchaseState"] = "canceledByUser";
                case .paymentInvalid: res["status"] = "error"
                    res["errorDescription"] = "Identificador do produto inválido."
                    res["purchaseState"] = "paymentInvalid"
                case .paymentNotAllowed: res["status"] = "error"
                    res["errorDescription"] = "Dispositivo sem autorização para fazer pagamentos."
                    res["purchaseState"] = "paymentNotAllowed"
                case .storeProductNotAvailable: res["status"] = "error"
                    res["errorDescription"] = "Produto indisponível na loja autal."
                    res["purchaseState"] = "storeProductNotAvailable"
                case .cloudServicePermissionDenied: res["status"] = "error"
                    res["errorDescription"] = "O acesso às informações do serviço em nuvem não é permitido."
                    res["purchaseState"] = "cloudServicePermissionDenied"
                case .cloudServiceNetworkConnectionFailed: res["status"] = "error"
                    res["errorDescription"] = "Não foi possível conectar-se à rede."
                    res["purchaseState"] = "cloudServiceNetworkConnectionFailed"
                case .cloudServiceRevoked: res["status"] = "error"
                    res["errorDescription"] = "O usuário revogou permissão para usar este serviço."
                    res["purchaseState"] = "cloudServiceRevoked"
                }
            }

            pluginResult = CDVPluginResult(
                    status: hasError ? CDVCommandStatus_ERROR : CDVCommandStatus_OK,
                    messageAs: res
            )

            self.commandDelegate!.send(
                    pluginResult,
                    callbackId: command.callbackId
            )
        }
    }

    @objc (restore:)
    func restore (command: CDVInvokedUrlCommand) {
        var res = [String: Any]()
        var hasError = false;

        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
            print("Results: \(results)")
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
                hasError = true
                res["status"] = "error"
                res["restoreState"] = "error"
                res["restoreDescription"] = "Falha ao restaurar."
            }
            else if results.restoredPurchases.count > 0 {
                for purchase in results.restoredPurchases where purchase.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                res["status"] = "success"
                res["restoreState"] = "restored"
                res["restoreDescription"] = "Restaurado com sucesso."
            }
            else {
                res["status"] = "nothing"
                res["restoreState"] = "nothing"
                res["restoreDescription"] = "Nenhum produto para restaurar."
            }

            pluginResult = CDVPluginResult(
                    status: hasError ? CDVCommandStatus_ERROR : CDVCommandStatus_OK,
                    messageAs: res
            )

            self.commandDelegate!.send(
                    pluginResult,
                    callbackId: command.callbackId
            )
        }
    }

    @objc (getReceipt:)
    func getReceipt (command: CDVInvokedUrlCommand) {
        SwiftyStoreKit.fetchReceipt(forceRefresh: true) { result in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
            var res = [String: Any]()

            switch result {
            case .success(let receiptData):
                let encryptedReceipt = receiptData.base64EncodedString(options: [])
                print("Fetch receipt success:\n\(encryptedReceipt)")
                res["receipt"] = encryptedReceipt;
                res["status"] = "success"
                pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_OK,
                        messageAs: res
                )
            case .error(let error):
                res["error"] = "Erro ao recuperar o recibo.";
                res["status"] = "error"
                pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_ERROR,
                        messageAs: res
                )
            }

            self.commandDelegate!.send(
                    pluginResult,
                    callbackId: command.callbackId
            )
        }
    }

    @objc (verifySubscription:)
    func verifySubscription (command: CDVInvokedUrlCommand) {
        var res = [String: Any]()
        var hasError = false;
        var sharedSecret = "";
        let productId = command.arguments[0] as! String
        switch productId {
            case "br.com.supportcomm.espiritualidade.premium":
                sharedSecret = "80cde1aeebab4d6389273165f7063b9c"
            case "br.com.supportcomm.vivomulher.2":
                sharedSecret = "2c05ea09c89749c4a8f352869a1848bd"
            case "br.com.takenet.dieta.semanal":
                sharedSecret = "e6087600b8064b5bb607768308312738"
            default: break
        }
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            var pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)

            switch result {
            case .success(let receipt):
                // Verify the purchase of a Subscription
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                        ofType: .autoRenewable,
                        productId: productId,
                        inReceipt: receipt)

                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                    res["purchaseState"] = "purchased";
                    if items.first!.quantity != nil {
                        res["purchaseDate"] = ZiranInAppPurchaseIOS.dateToStringFormatter.string(from: items.first!.purchaseDate)
                    }
                    if items.first!.isTrialPeriod != nil {
                        res["isTrialPeriod"] = items.first!.isTrialPeriod
                    }
                    if items.first!.quantity != nil {
                        res["quantity"] = items.first!.quantity
                    }
                    res["productId"] = productId
                    res["expiryDate"] = ZiranInAppPurchaseIOS.dateToStringFormatter.string(from: expiryDate)
                    res["latestReceipt"] = receipt["latest_receipt"];
                    print("\(productId) is valid until \(expiryDate)\n\(items)\n")
                case .expired(let expiryDate, let items):
                    res["purchaseState"] = "expired";
                    if items.first!.quantity != nil {
                        res["purchaseDate"] = ZiranInAppPurchaseIOS.dateToStringFormatter.string(from: items.first!.purchaseDate)
                    }
                    if items.first!.isTrialPeriod != nil {
                        res["isTrialPeriod"] = items.first!.isTrialPeriod
                    }
                    if items.first!.quantity != nil {
                        res["quantity"] = items.first!.quantity
                    }
                    res["productId"] = productId
                    res["expiryDate"] = ZiranInAppPurchaseIOS.dateToStringFormatter.string(from: expiryDate)
                    res["latestReceipt"] = receipt["latest_receipt"];
                    print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                case .notPurchased:
                    res["purchaseState"] = "notPurchased";
                    res["productId"] = productId
                    print("The user has never purchased \(productId)")
                }

            case .error(let error):
                hasError = true
                res["purchaseState"] = "unknown";
                res["productId"] = productId
            }

            pluginResult = CDVPluginResult(
                    status: hasError ? CDVCommandStatus_ERROR : CDVCommandStatus_OK,
                    messageAs: res
            )

            self.commandDelegate!.send(
                    pluginResult,
                    callbackId: command.callbackId
            )
        }
    }

    @objc(handlerTransactionOutsideTheApp:)
    func handlerTransactionOutsideTheApp (command: CDVInvokedUrlCommand) {
        SwiftyStoreKit.shouldAddStorePaymentHandler = { payment, product in
            return true
        }
    }
}

extension ZiranInAppPurchaseIOS {
    static var dateToStringFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
}