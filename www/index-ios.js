'use strict';
/*!
 *
 * Author: Alex Disler (alexdisler.com)
 * github.com/alexdisler/cordova-plugin-inapppurchase
 *
 * Licensed under the MIT license. Please see README for more information.
 *
 */

var ZiranUtils = {};

ZiranUtils.errors = {
  101: 'invalid argument - productIds must be an array of strings',
  102: 'invalid argument - productId must be a string',
  103: 'invalid argument - product type must be a string',
  104: 'invalid argument - receipt must be a string of a json',
  105: 'invalid argument - signature must be a string'
};

ZiranUtils.validArrayOfStrings = function (val) {
  return val && Array.isArray(val) && val.length > 0 && !val.find(function (i) {
    return !i.length || typeof i !== 'string';
  });
};

ZiranUtils.validString = function (val) {
  return val && val.length && typeof val === 'string';
};

ZiranUtils.chunk = function (array, size) {
  if (!Array.isArray(array)) {
    throw new Error('Invalid array');
  }

  if (typeof size !== 'number' || size < 1) {
    throw new Error('Invalid size');
  }

  var times = Math.ceil(array.length / size);
  return Array.apply(null, Array(times)).reduce(function (result, val, i) {
    return result.concat([array.slice(i * size, (i + 1) * size)]);
  }, []);
};

var ZiranInAppPurchase = {};

// This function is from https://github.com/AlexDisler/cordova-plugin-inapppurchase/blob/master/www/index-ios.js#L59
var nativeCall = function nativeCall(name) {
    var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

    return new Promise(function (resolve, reject) {
        window.cordova.exec(function (res) {
            resolve(res);
        }, function (err) {
            reject(err);
        }, 'ZiranInAppPurchaseIOS', name, args);
    });
};

ZiranInAppPurchase.initTransactions = function () {
  return new Promise(function (resolve, reject) {
      nativeCall('initTransactions').then(function (res) {
          resolve(res);
      }).catch(reject);
  })
};

ZiranInAppPurchase.getProductInfo = function (productId) {
    return new Promise(function (resolve, reject) {
        if (!ZiranUtils.validString(productId)) {
            reject(new Error(ZiranUtils.errors[102]));
        } else {
            nativeCall('getProductInfo', [productId]).then(function (res) {
                resolve(res);
            }).catch(reject);
        }
    });
};

ZiranInAppPurchase.purchase = function (productId) {
    return new Promise(function (resolve, reject) {
        if (!ZiranUtils.validString(productId)) {
            reject(new Error(ZiranUtils.errors[102]));
        } else {
            nativeCall('purchase', [productId]).then(function (res) {
                resolve(res);
            }).catch(reject);
        }
    });
};

ZiranInAppPurchase.restore = function () {
    return new Promise(function (resolve, reject) {
        nativeCall('restore').then(function (res) {
            resolve(res);
        }).catch(reject);
    });
};

ZiranInAppPurchase.verifySubscription = function (productId) {
    return new Promise(function (resolve, reject) {
        if (!ZiranUtils.validString(productId)) {
            reject(new Error(ZiranUtils.errors[102]));
        } else {
            nativeCall('verifySubscription', [productId]).then(function (res) {
                resolve(res);
            }).catch(reject);
        }
    });
};

ZiranInAppPurchase.getReceipt = function () {
    return new Promise(function (resolve, reject) {
        nativeCall('getReceipt').then(function (res) {
            resolve(res);
        }).catch(reject);
    });
};

ZiranInAppPurchase.handlerTransactionOutsideTheApp = function () {
    return new Promise(function (resolve, reject) {
        nativeCall('handlerTransactionOutsideTheApp').then(function (res) {
            resolve(res);
        }).catch(reject);
    });
};


module.exports = ZiranInAppPurchase;